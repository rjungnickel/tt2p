package tutorial;

import org.openspaces.core.GigaSpace;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class Car {

	private int speed;
	private GigaSpace gt;
	private Roxel rox;
	private World current;
	
	
	public Car(){
		gt = DataGridConnectionUtility.getSpace("MySpace");
		
		//generating random speed
		speed= 500 + (int)(Math.random() * ((1000 - 500) + 1));
		
		Roxel search= new Roxel();
		search.setFree(true);
		System.out.println("gigaspace: cont: "+gt.count(search));
		rox=gt.take(search);
		//write the current Roxel back so its occupied
		rox.setFree(false);
		gt.write(rox);
		
		current= gt.readById(World.class, 1);
	}
	
	
	@Transactional (propagation=Propagation.REQUIRED)
	public void moveTransaction() {
	/**
	 * start transaction
	 */
		//first get next Roxel
		Roxel nrox=nextRoxel();
		// second get current Roxel
		Roxel crox=gt.take(this.rox);
		
		crox.setFree(true);
		nrox.setFree(false);
		gt.write(crox);
		gt.write(nrox);
		rox=nrox;
	/**
	 * end transaction
	 */
	 }
	
	
	private Roxel nextRoxel(){
		
		int lastX=rox.getX();
		int lastY=rox.getY();
		Direction currDir=rox.getDir();
		
		if(currDir==Direction.north2South ){
			lastY=(lastY+1)%current.HEIGHT;
		}
		else if(currDir==Direction.west2East ){
			lastX=(lastX+1)%current.WIDTH;
		}
//		else {// kreuzung choice random way
//			Random ran = new Random();
//			if(ran.nextInt(2)==0)
//				lastX=(lastX+1)%current.WIDTH;
//			else	
//				lastY=(lastY+1)%current.HEIGHT;
//		}

		/**
		 * searching new Roxel
		 */
		Roxel search= new Roxel(lastX,lastY,currDir,true,null);
		
		System.out.println("lastX:"+lastX+"lastY:"+lastY);
		//another try
		Roxel newRoc=gt.take(search);

		if(newRoc==null){
			System.out.println("Roxel ist null");
		}
		
		return newRoc;
	}
	
	public void move(){
		
		
		while(true){
			
			try {
				Thread.sleep(speed);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.moveTransaction();

			
			System.out.println(rox.getX()+"/"+rox.getY());
			
			
		}
		
	}
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Car c = new Car();
		c.move();

	}

}
