package tutorial;

import java.util.List;

import org.openspaces.core.GigaSpace;



public class Main {

	public static void main(String[] args) {
		GigaSpace gt = DataGridConnectionUtility.getSpace("MySpace");
				
		World wrld=new World();
		List<Roxel> listOfRoxels = RoxelFactory.createRoxels(wrld);
		
		gt.writeMultiple(listOfRoxels.toArray());
		gt.write(wrld);
		
	}
		
}
