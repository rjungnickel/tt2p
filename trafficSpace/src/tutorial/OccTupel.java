package tutorial;

import com.gigaspaces.annotation.pojo.SpaceClass;
import com.gigaspaces.annotation.pojo.SpaceId;

@SpaceClass
public class OccTupel {
	
	private Integer x;
	private Integer y;
	private Long id;
	
	public OccTupel() {
		
	}
	
	public OccTupel(int x, int y, long id) {
		this.id=id;
		this.x=x;
		this.y=y;
		
	}
	
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	@SpaceId
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}

}
