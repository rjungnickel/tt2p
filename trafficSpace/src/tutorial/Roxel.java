package tutorial;

import com.gigaspaces.annotation.pojo.SpaceClass;
import com.gigaspaces.annotation.pojo.SpaceId;

@SpaceClass
	public class Roxel
	{
		private Integer x;
		private Integer y;
		private Direction dir;
		private Boolean free;
		private Integer ssn;
		private Boolean cross;
		
		public Roxel() {}
		
		public Roxel(Integer x, Integer y, Direction dir, Boolean free, Boolean cross) 
		{
			this.setX(x);
			this.setY(y);
			this.setDir(dir);
			this.setFree(free);
			this.setSsn(new String(x + "-" + y).hashCode());
			this.setCross(cross);
		}
		
		@SpaceId (autoGenerate=false)
		public Integer getSsn()
		{
			return this.ssn;
		}
		
		public void setSsn(Integer ssn)
		{
			this.ssn = ssn;
		}

		public Boolean isFree() {
			return free;
		}

		public void setFree(Boolean free) {
			this.free = free;
		}

		public Direction getDir() {
			return dir;
		}

		public void setDir(Direction dir) {
			this.dir = dir;
		}

		public Integer getX() {
			return x;
		}

		public void setX(Integer x) {
			this.x = x;
		}

		public Integer getY() {
			return y;
		}

		public void setY(Integer y) {
			this.y = y;
		}
		
		public Boolean getCross() {
			return cross;
		}

		public void setCross(Boolean cross) {
			this.cross = cross;
		}

		@Override
		public boolean equals(Object obj) {
			
			if(obj instanceof Roxel){
				Roxel other=(Roxel) obj;
				return other.x==this.x && other.y==this.y;
			}
			// TODO Auto-generated method stub
			return super.equals(obj);
		}

		@Override
		public int hashCode() {
			// TODO Auto-generated method stub
			return getSsn();
	
		}
		

	}

	enum Direction
	{
		north2South,
		west2East,
		toBeDefined
	}
