package tutorial;

import java.util.ArrayList;
import java.util.List;

public class RoxelFactory {
	
	public static List<Roxel> createRoxels(World wrld) 
	{
		List<Roxel> listOfRoxels = new ArrayList<Roxel>();
		
		for (int y = 0; y < wrld.HEIGHT; y++) {
			for (int x = 0; x < wrld.WIDTH;) {
				if ((y + 1) % 7 == 0 && y != 0)
				{
					if(x==6||x==13){ //Kreuzung
						listOfRoxels.add(new Roxel(x,y,Direction.north2South,true,true));
					}else
						listOfRoxels.add(new Roxel(x,y,Direction.west2East,true, false));
					
					System.out.println("1."+x+"/"+y);
					x++;
				}
				else
				{
					x += 6;
					if (x < wrld.WIDTH) {
						listOfRoxels.add(new Roxel(x,y,Direction.north2South,true, false));
						System.out.println("2."+x+"/"+y);
					}
					
					x++;
				}
			}
		}
		return listOfRoxels;
	}
	
}
