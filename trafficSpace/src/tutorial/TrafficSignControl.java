package tutorial;

import org.openspaces.core.GigaSpace;

public class TrafficSignControl {

	GigaSpace gt;
	int controlSpeed=1000;
	
	
	public TrafficSignControl(){
		gt = DataGridConnectionUtility.getSpace("MySpace");
		
	}
	
	public void control(){
		
		while(true){
			try {
				Thread.sleep(controlSpeed);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			Roxel ser=new Roxel();
			ser.setCross(true);
			ser.setFree(true);
			Roxel[] cross= gt.readMultiple(ser);
			for(Roxel a:cross){
				Roxel ro= gt.take(a);
				Direction dir=ro.getDir();
				Direction newd;
				if(dir==Direction.north2South)
					newd=Direction.west2East;
				else
					newd=Direction.north2South;
				
				ro.setDir(newd);
				gt.write(ro);
				
			}
			
			
		}
		
		
	}
	
	
	
	
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new TrafficSignControl().control();

	}

}
